<?php
	
	/**
	 * Retrieve all blog posts or a specific post 
	 * 
	 */
	class blog extends connect
	{
		/**
		 * Array of post objects
		 * 
		 * @var array 
		 */
		 public $posts = array();
		
		/**
		 * An object representing a single post
		 * 
		 * @var obj
		 */
		public $post; 
		
		function __construct()
		{
			parent::__construct();
		}
		
		public function getMultiplePosts($start,$limit)
		{
			try
			{
				$limit = intval($limit);
				$qry = "SELECT `id`,`datePosted`, `headline`, `body` FROM `blog` WHERE `visible` = 1 AND `id`> ? ORDER BY `datePosted` DESC LIMIT $limit ";
				$p = $this->db->prepare($qry);
				$p->execute(array($start));
				
				foreach($p->fetchALL() as $post)
				{
					$sanitizeVars = array('datePosted','headline','id');
					foreach($sanitizeVars as $var)
					{
						$post->$var = htmlentities($post->$var);	
					}
					
					$this->posts[] = $post;
				}
			}
			catch(Exception $e)
			{
				echo $e;
				
				throw new Exception("Unable to collect blog posts",1);
			}
			
		}
		
		public function getPost($id)
		{
			try
			{
				$p = $this->db->prepare("SELECT `id`,`datePosted`, `headline`, `body` FROM `blog` WHERE `visible` = '1' AND `id` = ? LIMIT 1");
				$p->execute(array($id));
				$this->post = $p->fetch();
			}
			catch(Exception $e)
			{
				throw new Exception("Unable to collect data for post $id",1);
			}
			
		}
		
	}
