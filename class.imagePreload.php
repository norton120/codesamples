<?php

	/**
	 * Preload all project images to help speed up the rest of the experience.
	 * 
	 */
	class preload
	{
		/**
		 * an array containing all image filenames
		 * 
		 * @var array;
		 */
		protected $images = array();
	
		function __construct()
		{
			$this->getImages("../public/images");
		}	
		
		public function getImages($location)
		{
			
			$files = scandir($location);
			
			foreach ($files as $file)
			{
				//remove . and ..
				if($file == "." || $file == "..")
					continue;
				
				//add files to array
				if(is_file($location.'/'.$file))
				{
					$this->images[] = $file;
				}
				
				//make it recursive
				elseif(is_dir($file))
				{
					$this->getImages($file);	
				}
			}		
		}
		
		/**
		 * Returns the array of images
		 * 
		 * @return array
		 */
		public function returnImages()
		{
			return $this->images;
		}
	}	
	